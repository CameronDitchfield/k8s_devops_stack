resource "azurerm_resource_group" "minikube" {
  name     = var.resource_group_name
  location = var.location
  tags     = var.tags
}

resource "random_string" "fqdn" {
  length  = 6
  special = false
  upper   = false
  number  = false
}

resource "azurerm_virtual_network" "minikube" {
  name                = "k8s-devops-stack-vnet"
  resource_group_name = azurerm_resource_group.minikube.name
  location            = var.location
  address_space       = ["10.0.0.0/16"]
  tags                = var.tags
}

resource "azurerm_subnet" "minikube" {
  name                 = "k8s-devops-stack-subnet"
  resource_group_name  = azurerm_resource_group.minikube.name
  virtual_network_name = azurerm_virtual_network.minikube.name
  address_prefixes     = ["10.0.2.0/24"]
}

resource "azurerm_public_ip" "minikube" {
  name                = "k8s-devops-stack-public-ip"
  location            = var.location
  resource_group_name = azurerm_resource_group.minikube.name
  sku                 = "Standard"
  allocation_method   = "Static"
  domain_name_label   = random_string.fqdn.result
  tags                = var.tags
}

resource "azurerm_network_security_group" "minikube" {
  name                = "k8s-devops-stack-nsg"
  location            = azurerm_resource_group.minikube.location
  resource_group_name = azurerm_resource_group.minikube.name

  security_rule {
    name                       = "ssh"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "*"
    destination_address_prefix = "*"
    source_address_prefix      = var.home_ip
  }

  tags = var.tags
}

resource "azurerm_subnet_network_security_group_association" "minikube" {
  subnet_id                 = azurerm_subnet.minikube.id
  network_security_group_id = azurerm_network_security_group.minikube.id
}

resource "azurerm_network_interface" "minikube" {
  name                = "k8s-devops-stack-nic"
  location            = azurerm_resource_group.minikube.location
  resource_group_name = azurerm_resource_group.minikube.name

  ip_configuration {
    name                          = "minikube_config"
    subnet_id                     = azurerm_subnet.minikube.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.minikube.id
  }
}

resource "azurerm_linux_virtual_machine" "minikube" {
  name                       = "k8s-devops-stack-vm"
  location                   = azurerm_resource_group.minikube.location
  resource_group_name        = azurerm_resource_group.minikube.name
  network_interface_ids      = [azurerm_network_interface.minikube.id]
  size                       = "Standard_D4s_v4"
  admin_username             = "cameron"
  allow_extension_operations = true

  admin_ssh_key {
    username   = "cameron"
    public_key = file("~/.ssh/azure_we.pem.pub")
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  tags = var.tags
}

resource "azurerm_virtual_machine_extension" "minikube" {
  name                 = "k8s-devops-stack-extension"
  publisher            = "Microsoft.Azure.Extensions"
  type                 = "CustomScript"
  type_handler_version = "2.0"
  virtual_machine_id   = azurerm_linux_virtual_machine.minikube.id

  settings = jsonencode({
    "fileUris"         = ["https://bitbucket.org/CameronDitchfield/k8s_devops_stack/raw/2ccfcc41f56ff8f0ccc33c5a9ff9857763247847/install_minikube.sh"]
    "commandToExecute" = "sh install_minikube.sh"
  })

  tags = var.tags
}