variable "location" {
  description = "The location where resources will be created"
  default     = "westeurope"
}

variable "tags" {
  description = "A map of the tags to use for the resources that are deployed"
  type        = map(string)

  default = {
    environment = "k8s-devops-stack"
  }
}

variable "resource_group_name" {
  description = "The name of the resource group in which the resources will be created"
  default     = "k8s-devops-stack"
}

variable "application_port" {
  description = "The port that you want to expose to the external load balancer"
  default     = 8080
}

variable "admin_user" {
  description = "User name to use as the admin account on the VMs that will be part of the VM Scale Set"
  default     = "cameron"
}

variable "home_ip" {
  default = "81.100.212.63"
}