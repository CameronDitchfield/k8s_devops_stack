terraform {
  backend "azurerm" {
    resource_group_name  = "k8s-devops-stack-storage"
    storage_account_name = "k8sdevopsstackstorage"
    container_name       = "tfstate"
    key                  = "infra.terraform.tfstate"
  }
}