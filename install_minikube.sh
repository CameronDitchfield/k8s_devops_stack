#!/bin/bash -xv

ADMIN_USER="cameron"

yum install -y virtualbox
yum install -y docker.io
yum install -y curl

systemctl start docker
systemctl enable docker

curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
kubectl version --client

curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
install minikube-linux-amd64 /usr/local/bin/minikube
minikube version

groupadd docker
usermod -aG docker $ADMIN_USER

curl https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get-helm-3 > get_helm.sh
chmod 700 get_helm.sh
./get_helm.sh
/bin/su -c "helm repo add bitnami https://charts.bitnami.com/bitnami" - $ADMIN_USER

/bin/su -c "git clone https://CameronDitchfield@bitbucket.org/CameronDitchfield/k8s_devops_stack.git" - $ADMIN_USER

/bin/su -c "minikube start --vm-driver=virtualbox" - $ADMIN_USER

# Create jenkins namespace and persistent volume
/bin/su -c "kubectl apply -f ./k8s_devops_stack/jenkins/jenkins-namespace.yaml" - $ADMIN_USER
/bin/su -c "kubectl apply -f ./k8s_devops_stack/jenkins/jenkins-volume.yaml" - $ADMIN_USER

# Launch jenkins in the cluster
/bin/su -c "helm install jenkins -f ./k8s_devops_stack/jenkins/values.yaml bitnami/jenkins --namespace jenkins --set service.type=NodePort" - $ADMIN_USER


